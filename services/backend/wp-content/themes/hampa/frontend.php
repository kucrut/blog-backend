<?php
/**
 * Hampa's frontend stuff
 *
 * @package Hampa
 */

declare( strict_types = 1 );

namespace Hampa\Frontend;

/**
 * Frontend bootstrapper
 *
 * @since 0.0.1
 *
 * @return void
 */
function bootstrap(): void {
	add_action( 'wp', __NAMESPACE__ . '\\no_frontend' );
}

/**
 * Redirect frontend requests
 *
 * @since 0.0.1
 *
 * This redirectes all frontend requests to
 * - dashboard, if logged in, or
 * - login page, if not logged in.
 *
 * @return void
 */
function no_frontend(): void {
	if (
		$_SERVER['REQUEST_METHOD'] === 'HEAD' ||
		defined( 'WP_CLI' ) ||
		wp_is_json_request() ||
		is_admin() ||
		strpos( $_SERVER['PHP_SELF'], '/wp-login.php' ) !== false
	) {
		return;
	}

	$redirect_url = is_user_logged_in() ? admin_url() : wp_login_url();
	wp_safe_redirect( $redirect_url );
	exit;
}
