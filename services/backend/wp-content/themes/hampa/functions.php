<?php
/**
 * Hampa's functionalities
 *
 * @package Hampa
 */

declare( strict_types = 1 );

namespace Hampa;

require_once __DIR__ . '/frontend.php';

Frontend\bootstrap();
