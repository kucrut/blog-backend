<?php
/**
 * Plugin Name: Attachment Taxonomy
 * Description: Custom taxonomy for attachments.
 * Version: 0.1.0
 * Author: Dzikri Aziz
 * Author URI: https://dz.aziz.im
 * License: GPL v2
 *
 * @package Dz_Attachment_Taxonomy
 */

namespace Dz\Attachment_Taxonomy;

use WP_Post;
use WP_REST_Attachments_Controller;
use WP_REST_Request;

/**
 * Check whether current user can assign all terms sent with the current request.
 *
 * @param WP_REST_Request $request The request object with post and terms data.
 * @return bool Whether the current user can assign the provided terms.
 */
function check_assign_terms_permission( $request ) {
	$taxonomies = wp_list_filter( get_object_taxonomies( 'attachment', 'objects' ), array( 'show_in_rest' => true ) );

	foreach ( $taxonomies as $taxonomy ) {
		$base = ! empty( $taxonomy->rest_base ) ? $taxonomy->rest_base : $taxonomy->name;

		if ( ! isset( $request[ $base ] ) ) {
			continue;
		}

		foreach ( (array) $request[ $base ] as $term_id ) {
			// Invalid terms will be rejected later.
			if ( ! get_term( $term_id, $taxonomy->name ) ) {
				continue;
			}

			if ( ! current_user_can( 'assign_term', (int) $term_id ) ) {
				return false;
			}
		}
	}

	return true;
}

/**
 * Register category taxonomy for attachments
 */
function register() {
	register_taxonomy_for_object_type( 'category', 'attachment' );
}

/**
 * Check permission to to assign terms for an attachment
 *
 * Stolen from https://wordpress.org/plugins/attachment-taxonomies
 *
 * @param WP_REST_Response|WP_HTTP_Response|WP_Error|mixed $response Result to send to the client.
 *                                                                   Usually a WP_REST_Response or WP_Error.
 * @param array                                            $handler  Route handler used for the request.
 * @param WP_REST_Request                                  $request  Request used to generate the response.
 *
 * @return WP_REST_Response|WP_HTTP_Response|WP_Error|mixed Unmodified $response, or WP_Error to force failure.
 */
function rest_check_assign_terms_permission( $response, $handler, $request ) {
	if ( ! isset( $handler['permission_callback'] ) || ! is_array( $handler['permission_callback'] ) ) {
		return $response;
	}

	if (
		! $handler['permission_callback'][0] instanceof WP_REST_Attachments_Controller ||
		! in_array( $handler['permission_callback'][1], [ 'create_item_permissions_check', 'update_item_permissions_check' ], true )
	) {
			return $response;
	}

	$assign_terms_check = check_assign_terms_permission( $request );

	if ( is_wp_error( $assign_terms_check ) ) {
		return $assign_terms_check;
	}

	return $response;
}

/**
 * Assigning terms to an attachment on REST requests
 *
 * @param WP_Post         $attachment Inserted or updated attachment object.
 * @param WP_REST_Request $request    Request object.
 */
function rest_handle_attachment_terms( $attachment, $request ) {
	$taxonomies = wp_list_filter( get_object_taxonomies( $attachment->post_type, 'objects' ), array( 'show_in_rest' => true ) );

	foreach ( $taxonomies as $taxonomy ) {
		$base = ! empty( $taxonomy->rest_base ) ? $taxonomy->rest_base : $taxonomy->name;

		if ( ! isset( $request[ $base ] ) ) {
			continue;
		}

		$result = wp_set_object_terms( $attachment->ID, $request[ $base ], $taxonomy->name );

		if ( is_wp_error( $result ) ) {
			return $result;
		}
	}
}

add_action( 'init', __NAMESPACE__ . '\\register' );
add_filter( 'rest_request_before_callbacks', __NAMESPACE__ . '\\rest_check_assign_terms_permission', 10, 3 );
add_action( 'rest_after_insert_attachment', __NAMESPACE__ . '\\rest_handle_attachment_terms', 10, 2 );
