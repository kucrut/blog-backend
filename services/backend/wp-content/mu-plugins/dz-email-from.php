<?php
/**
 * Plugin Name: Email From
 * Description: Use admin email as email address to send from, and site name as sender name.
 * Version: 0.1.0
 * Author: Dzikri Aziz
 * Author URI: https://dz.aziz.im
 * License: GPL v2
 *
 * @package Dz_Email_From
 */

add_filter( 'wp_mail_from', fn() => get_option( 'admin_email' ) );
add_filter( 'wp_mail_from_name', fn() => get_bloginfo( 'name' ) );
