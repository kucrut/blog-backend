<?php
/**
 * Plugin Name: Allow API login
 * Description: Allow API login when two factor is used.
 * Version: 0.1.0
 * Author: Dzikri Aziz
 * Author URI: https://dz.aziz.im
 * License: GPL v2
 *
 * @package Dz_Two_Factor
 */

add_filter( 'two_factor_user_api_login_enable', '__return_true' );
