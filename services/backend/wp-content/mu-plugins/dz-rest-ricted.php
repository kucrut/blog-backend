<?php
/**
 * Plugin Name: REST-ricted
 * Description: Restrict REST API to logged-in users only.
 * Version: 0.1.0
 * Author: Dzikri Aziz
 * Author URI: https://dz.aziz.im
 * License: GPL v2
 *
 * @package Dz_Rest_Ricted
 */

// phpcs:disable PEAR.Functions.FunctionCallSignature.ContentAfterOpenBracket
// phpcs:disable PEAR.Functions.FunctionCallSignature.CloseBracketLine
// phpcs:disable PEAR.Functions.FunctionCallSignature.MultipleArguments
// phpcs:disable WordPress.Security.NonceVerification.Recommended

add_filter( 'rest_authentication_errors', function ( $errors ) {
	if ( is_user_logged_in() || is_wp_error( $errors ) ) {
		return $errors;
	}

	return new WP_Error( 'rest-ricted', '⛔', [ 'status' => 401 ] );
} );
