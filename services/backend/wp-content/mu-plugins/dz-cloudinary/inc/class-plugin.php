<?php
/**
 * Main functions
 *
 * @package Dz_Cloudinary
 */

declare( strict_types = 1 );

namespace Dz\Cloudinary;

use WP_CLI;

/**
 * Plugin main class
 */
class Plugin {
	/**
	 * Class instances
	 *
	 * @since 0.1.0
	 * @var array
	 */
	private static $instances = [];

	/**
	 * Constructor
	 *
	 * @since 0.1.0
	 */
	protected function __construct() {
		add_action( 'wp_loaded', [ $this, 'init' ] );
	}

	/**
	 * Get upload folder
	 *
	 * @since 0.1.0
	 *
	 * @return string|null
	 */
	private function generate_upload_folder_name(): string {
		static $folder;

		if ( ! empty( $folder ) ) {
			return $folder;
		}

		$env_type = defined( 'WP_ENVIRONMENT_TYPE' ) ? WP_ENVIRONMENT_TYPE : 'local';
		$url_parts = wp_parse_url( home_url() );

		unset( $url_parts['scheme'] );
		$url_parts = array_map( fn ( $item ) => trim( $item, '/' ), $url_parts );

		$folder = join( '/', [ ...$url_parts, $env_type ] );

		return $folder;
	}

	/**
	 * Get class instance
	 *
	 * @since 0.1.0
	 *
	 * @return Plugin
	 */
	public static function get_instance(): Plugin {
		$cls = static::class;

		if ( ! isset( self::$instances[ $cls ] ) ) {
			self::$instances[ $cls ] = new static();
		}

		return self::$instances[ $cls ];
	}

	/**
	 * Init plugin
	 *
	 * @since 0.1.0
	 */
	public function init(): void {
		$url = Settings\get_option_value( 'api_env_var' );

		if ( empty( $url ) ) {
			return;
		}

		$api = new API( $url, $this->generate_upload_folder_name(), 'private' );

		Cron\bootstrap( $api );
		new Sync( $api );
		new Overrides( $api );

		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			require_once __DIR__ . '/class-commands.php';

			WP_CLI::add_command( 'dz-cloudinary', new Commands( $api ) );
		}
	}
}
