<?php
/**
 * Plugin Name: Cloudinary Integration
 * Version: 0.1.0
 * Author: Dzikri Aziz
 * Author URI: https://dz.aziz.im
 * License: GPL v2
 *
 * @package Dz_Cloudinary
 */

declare( strict_types = 1 );

namespace Dz\Cloudinary;

use Dz\Cloudinary\Meta;

/**
 * Overrides
 */
class Overrides {
	/**
	 * Constructor
	 *
	 * @since 0.1.0
	 *
	 * @param API $api API class instance.
	 */
	public function __construct( protected API $api ) {
		add_filter( 'image_downsize', [ $this, 'override_image_downsize' ], 10, 3 );
		add_filter( 'image_get_intermediate_size', [ $this, 'override_image_intermediate_size_url' ], 10, 3 );
		add_filter( 'wp_get_attachment_image_src', [ $this, 'override_attachment_image_src' ], 10, 2 );
		add_filter( 'wp_get_attachment_url', [ $this, 'override_attachment_url' ], 10, 2 );
	}

	/**
	 * Override image attachment src URL
	 *
	 * @since 0.1.0
	 *
	 * @param array|false $image Image data.
	 * @param int         $id    Attachment ID.
	 *
	 * @return array
	 */
	public function override_attachment_image_src( array|false $image, int $id ): array|false {
		if ( $image === false ) {
			return false;
		}

		$public_id = Meta\get_data( $id, 'public_id' );

		if ( empty( $public_id ) ) {
			return $image;
		}

		$image[0] = $this->api->transform_image( $public_id, $image[1], $image[2] );

		return $image;
	}

	/**
	 * Override attachment URL
	 *
	 * @since 0.1.0
	 *
	 * @param string $url Attachment URL.
	 * @param int    $id  Attachment ID.
	 *
	 * @return string
	 */
	public function override_attachment_url( string $url, int $id ): string {
		$data = Meta\get_data( $id );

		if ( empty( $data ) ) {
			return $url;
		}

		$url = is_ssl() ? $data['secure_url'] : $data['url'];

		return $url;
	}

	/**
	 * Override intermediate image size's URL
	 *
	 * @since 0.1.0
	 *
	 * @param array $image Image data.
	 * @param int   $id    Attachment ID.
	 *
	 * @return array
	 */
	public function override_image_intermediate_size_url( array $image, int $id ): array {
		$public_id = Meta\get_data( $id, 'public_id' );

		if ( empty( $public_id ) ) {
			return $image;
		}

		$image['url'] = $this->api->transform_image( $public_id, $image['width'], $image['height'] );

		return $image;
	}

	/**
	 * Override image downsize
	 *
	 * @since 0.1.0
	 *
	 * @param bool|array   $downsize Whether to short-circuit the image downsize.
	 * @param int          $id       Attachment ID for image.
	 * @param string|int[] $size     Requested image size. Can be any registered image size name, or
	 *                               an array of width and height values in pixels (in that order).
	 *
	 * @return mixed
	 */
	public function override_image_downsize( $downsize, int $id, $size ) {
		$data = Meta\get_data( $id );

		if ( empty( $data ) ) {
			return $downsize;
		}

		$intermediate = image_get_intermediate_size( $id, $size );

		if ( $intermediate ) {
			return [
				$intermediate['url'],
				$intermediate['width'],
				$intermediate['height'],
				true,
			];
		}

		// TODO.

		return $downsize;
	}
}
