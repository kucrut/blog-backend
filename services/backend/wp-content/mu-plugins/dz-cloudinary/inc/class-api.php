<?php
/**
 * Cloudinary API
 *
 * @package Dz_Cloudinary
 */

declare( strict_types = 1 );

namespace Dz\Cloudinary;

use Cloudinary\Api\ApiResponse;
use Cloudinary\Api\Exception\ApiError;
use Cloudinary\Cloudinary as Cld;

/**
 * API
 */
class API {
	/**
	 * Cloudinary API instance
	 *
	 * @since 0.1.0
	 *
	 * @var Cld
	 */
	protected Cld $instance;

	/**
	 * Constructor
	 *
	 * @param string $url           Cloudinary URL.
	 * @param string $folder        Folder name where the items should be stored.
	 * @param string $delivery_type Delivery type.
	 *
	 * @since 0.1.0
	 */
	public function __construct( protected string $url, protected string $folder, protected string $delivery_type ) {
		$this->instance = new Cld( $url );
	}

	/**
	 * Delete item
	 *
	 * @since 0.1.0
	 *
	 * @param string $public_id Item's public ID.
	 * @param array  $args      Extra arguments to be passed to the action hook.
	 *
	 * @return ApiResponse|ApiError Delete response or error.
	 */
	public function delete( string $public_id, array $args = [] ): ApiResponse|ApiError {
		try {
			$response = $this->instance->uploadApi()->destroy( $public_id, [ 'type' => $this->delivery_type ] );

			/**
			 * Fire after the file has been deleted in Cloudinary.
			 *
			 * @param ApiResponse $response Delete response.
			 * @param array       $args     Extra arguments.
			 */
			do_action( 'dz_cloudinary_deleted', $response, $args );

			return $response;
		} catch ( ApiError $error ) {
			/**
			 * Fire when delete throws error
			 *
			 * @param ApiError $error Upload error.
			 */
			do_action( 'dz_cloudinary_delete_error', $error );

			return $error;
		}
	}

	/**
	 * Transform image
	 *
	 * NOTE: This is using string transformation for now because the PHP SDK doesn't
	 * seem to have the auto crop functionality yet.
	 *
	 * @since 0.1.0
	 * @todo Support predefined transformations?
	 *
	 * @param string $public_id Resource's public ID.
	 * @param int    $width     Requested image width.
	 * @param int    $height    Requested image height.
	 *
	 * @return string Image url after transformation.
	 */
	public function transform_image( string $public_id, int $width, int $height ): string {
		return (string) $this->instance
			->image( $public_id )
			->addTransformation( "c_auto,g_auto,w_{$width},h_{$height}" )
			->deliveryType( $this->delivery_type )
			->signUrl();
	}


	/**
	 * Upload image to Cloudinary
	 *
	 * @since 0.1.0
	 *
	 * @param string $file Path to file.
	 * @param array  $args Extra arguments to be passed to the action hook.
	 *
	 * @return ApiResponse|ApiError Upload response or error.
	 */
	public function upload( string $file, array $args = [] ): ApiResponse|ApiError {
		try {
			$response = $this->instance->uploadApi()->upload(
				$file,
				[
					'folder' => $this->folder,
					'type' => $this->delivery_type,
				]
			);

			/**
			 * Fire after the file has been uploaded to Cloudinary.
			 *
			 * @param ApiResponse $response Upload response.
			 * @param array       $args     Extra arguments.
			 */
			do_action( 'dz_cloudinary_uploaded', $response, $args );

			return $response;
		} catch ( ApiError $error ) {
			/**
			 * Fire when upload throws error
			 *
			 * @param ApiError $err Upload error.
			 */
			do_action( 'dz_cloudinary_upload_error', $error );

			return $error;
		}
	}
}
