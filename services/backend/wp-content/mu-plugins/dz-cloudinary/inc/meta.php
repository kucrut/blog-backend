<?php
/**
 * Meta
 *
 * @package Dz_Cloudinary
 */

declare( strict_types = 1 );

namespace Dz\Cloudinary\Meta;

use Cloudinary\Api\ApiResponse;

const META_KEY_DATA = 'dz_cloudinary_data';

/**
 * Bootstrap
 *
 * @since 0.1.0
 *
 * @return void
 */
function bootstrap(): void {
	add_action( 'dz_cloudinary_uploaded', __NAMESPACE__ . '\\update_data_on_upload', 10, 2 );
}

/**
 * Get attachment's Cloudinary data
 *
 * @since 0.1.0
 *
 * @param int    $id  Attachment ID.
 * @param string $key Individual data key to get.
 *
 * @return mixed
 */
function get_data( int $id, string $key = '' ): mixed {
	$data = get_post_meta( $id, META_KEY_DATA, true );

	if ( ! is_array( $data ) || empty( $data ) ) {
		return null;
	}

	if ( $key === '' ) {
		return $data;
	}

	return isset( $data[ $key ] ) ? $data[ $key ] : null;
}

/**
 * Update attachment's Cloudinary data
 *
 * @since 0.1.0
 *
 * @param int         $id       Attachment ID.
 * @param ApiResponse $response Upload response.
 *
 * @return int|bool
 */
function update_data( int $id, ApiResponse $response ): int|bool {
	// phpcs:ignore Generic.Commenting.Todo.TaskFound
	// TODO: More granular metadata saving?
	return update_post_meta( $id, META_KEY_DATA, (array) $response );
}

/**
 * Update attachment data on successful upload
 *
 * @since 0.1.0
 *
 * @param ApiResponse $response API response.
 * @param mixed       $args     Extra arguments passed to API::upload().
 */
function update_data_on_upload( ApiResponse $response, $args ) {
	if ( is_array( $args ) && ! empty( $args['id'] ) ) {
		update_data( $args['id'], $response );
	}
}
