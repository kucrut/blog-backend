<?php
/**
 * Sync images to Cloudinary
 *
 * @package Dz_Cloudinary
 */

declare( strict_types = 1 );

namespace Dz\Cloudinary;

use Dz\Cloudinary\Cron;
use Dz\Cloudinary\Meta;

/**
 * Sync
 */
class Sync {
	/**
	 * Constructor
	 *
	 * @since 0.1.0
	 *
	 * @param API $api API class instance.
	 */
	public function __construct( protected API $api ) {
		add_action( 'add_attachment', [ $this, 'handle_add_attachment' ] );
		add_action( 'delete_attachment', [ $this, 'handle_delete_attachment' ] );
	}

	/**
	 * `delete_attachment` hook handler
	 *
	 * @since 0.1.0
	 * @todo Maybe delete via a cron job to avoid timeout.
	 *
	 * @param int $id Post ID.
	 */
	public function handle_delete_attachment( int $id ): void {
		$public_id = Meta\get_data( $id, 'public_id' );

		if ( $public_id ) {
			Cron\schedule_delete( $public_id, [ 'id' => $id ] );
		}
	}

	/**
	 * `add_attachment` hook handler
	 *
	 * @since 0.1.0
	 *
	 * @param int $id Attachment ID.
	 */
	public function handle_add_attachment( int $id ): void {
		// phpcs:ignore Generic.Commenting.Todo.TaskFound
		// TODO: Sync non-images as well.
		if ( ! wp_attachment_is_image( $id ) ) {
			return;
		}

		$this->api->upload( get_attached_file( $id ), [ 'id' => $id ] );
	}
}
