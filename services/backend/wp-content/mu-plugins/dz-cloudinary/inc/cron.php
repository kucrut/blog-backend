<?php
/**
 * Cron
 *
 * @package Dz_Cloudinary
 */

namespace Dz\Cloudinary\Cron;

use Dz\Cloudinary\API;
use WP_Error;

const EVENT_DELETE_ITEM = 'dz_cloudinary_delete_item';

/**
 * Bootstrap
 *
 * @param API $api API instance.
 *
 * @since 0.1.0
 */
function bootstrap( API $api ): void {
	add_action(
		EVENT_DELETE_ITEM,
		fn ( string $public_id, array $args = [] ) => $api->delete( $public_id, $args ),
		10,
		2
	);
}

/**
 * Schedule deletion
 *
 * @since 0.1.0
 *
 * @param string $public_id Cloudinary item public ID.
 * @param array  $args      Extra arguments to be passed to API::delete().
 *
 * @return boolean|WP_Error
 */
function schedule_delete( string $public_id, array $args = [] ): bool|WP_Error {
	return wp_schedule_single_event( time(), EVENT_DELETE_ITEM, [ $public_id, $args ] );
}
