<?php
/**
 * CLI commands
 *
 * @package  Dz_Cloudinary
 */

declare( strict_types = 1 );

namespace Dz\Cloudinary;

use Cloudinary\Api\ApiResponse;
use Cloudinary\Api\Exception\ApiError;
use Dz\Cloudinary\API;
use WP_CLI;
use WP_Post;
use WP_Query;

/**
 * CLI Commands
 */
class Commands {

	/**
	 * Constructor
	 *
	 * @since 0.1.0
	 *
	 * @param API $api API instance.
	 */
	public function __construct( protected API $api ) {}

	/**
	 * Sync image attachments to Cloudinary
	 *
	 * @todo: Improve performance.
	 *
	 * ## EXAMPLES
	 *
	 *     wp dz-cloudinary sync
	 *
	 * @when after_wp_load
	 */
	public function sync() {
		$query = new WP_Query(
			[
				'meta_compare' => 'NOT EXISTS',
				'meta_key' => Meta\META_KEY_DATA,
				'post_status' => 'inherit',
				'post_type' => 'attachment',
				'posts_per_page' => -1,
			]
		);

		if ( ! $query->have_posts() ) {
			WP_CLI::success( __( 'No unsynced image attachments found.', 'dz' ) );
			return;
		}

		$filtered = array_filter(
			$query->posts,
			fn ( WP_Post $post ) => str_starts_with( $post->post_mime_type, 'image' )
		);

		if ( empty( $filtered ) ) {
			WP_CLI::success( __( 'No unsynced image attachments found.', 'dz' ) );
			return;
		}

		// translators: %d is the number of image attachments found.
		WP_CLI::log( sprintf( __( 'Found %d image attachments to sync.', 'dz' ), count( $filtered ) ) );

		foreach ( $filtered as $item ) {
			$file = get_attached_file( $item->ID );

			if ( ! file_exists( $file ) ) {
				// translators: %1$d = attachment id, %2$s = attachment file.
				WP_CLI::warning( sprintf( __( 'Original file of attachment %1$d (%2$s) could not be found, skipping.', 'dz' ), $item->ID, $file ) );
				continue;
			}

			$result = $this->api->upload( $file, [ 'id' => $item->ID ] );

			if ( $result instanceof ApiResponse ) {
				// translators: %d = attachment id.
				WP_CLI::success( sprintf( __( 'Attachment %d synced.', 'dz' ), $item->ID ) );
			} elseif ( $result instanceof ApiError ) {
				// translators: %d = attachment id.
				WP_CLI::warning( sprintf( __( 'Failed to upload attachment %d.', 'dz' ), $item->ID ) );
			}
		}
	}
}
