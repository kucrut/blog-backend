<?php
/**
 * Settings page
 *
 * @package Dz_Cloudinary
 */

declare( strict_types = 1 );

namespace Dz\Cloudinary\Settings;

const GROUP = 'media';
const PREFIX = 'dz_cloudinary';

/**
 * Bootstrap
 *
 * @since 0.1.0
 */
function bootstrap(): void {
	add_action( 'admin_init', __NAMESPACE__ . '\\register_section_and_fields' );
}

/**
 * Get option fields
 *
 * @since 0.1.0
 *
 * @return array
 */
function get_fields(): array {
	return [
		[
			'id' => get_option_name( 'api_env_var' ),
			'title' => __( 'API environment variable', 'dz' ),
			'args' => [
				'default' => '',
				'description' => __( 'This can be found on your Cloudinary console.', 'dz' ),
				'sanitize_callback' => __NAMESPACE__ . '\\sanitize_api_env_var',
				'type' => 'string',
				'show_in_rest' => [
					'name' => get_option_name( 'api_env_var' ),
					'schema' => [
						'type' => 'string',
					],
				],
			],
		],
	];
}

/**
 * Get option name
 *
 * @since 0.1.0
 *
 * @param string $key Option key/field ID.
 *
 * @return string
 */
function get_option_name( string $key ): string {
	return sprintf( '%s_%s', PREFIX, $key );
}

/**
 * Get option value
 *
 * @since 0.1.0
 *
 * @param string $id Field ID.
 *
 * @return mixed
 */
function get_option_value( string $id ) {
	return get_option( get_option_name( $id ) );
}

/**
 * Register settings section & fields
 *
 * @since 0.1.0
 */
function register_section_and_fields(): void {
	add_settings_section(
		PREFIX,
		'Cloudinary',
		null,
		GROUP
	);

	foreach ( get_fields() as $field ) {
		register_setting( GROUP, $field['id'], $field['args'] );

		add_settings_field(
			$field['id'],
			$field['title'],
			__NAMESPACE__ . '\\render_field',
			GROUP,
			PREFIX,
			[
				'field' => $field,
				'label_for' => $field['id'],
			]
		);
	}
}

/**
 * Render field description
 *
 * @since 0.1.0
 * @param array $field Field data.
 */
function render_field_description( array $field ): void {
	if ( ! empty( $field['args']['description'] ) ) {
		printf(
			'<p class="description" id="%s">%s</p>',
			esc_attr( "{$field['id']}_desc" ),
			esc_html( $field['args']['description'] )
		);
	}
}

/**
 * Render field
 *
 * @since 0.1.0
 *
 * @param array $args Field arguments.
 */
function render_field( array $args ): void {
	printf(
		'<input class="regular-text" id="%1$s" name="%1$s" type="text" value="%2$s"%3$s>',
		esc_attr( $args['label_for'] ),
		esc_attr( get_option( $args['field']['id'] ) ),
		! empty( $args['field']['args']['description'] )
			? sprintf( ' aria-describedby="%s"', esc_attr( "{$args['field']['id']}_desc" ) )
			: ''
	);

	render_field_description( $args['field'] );
}

/**
 * Sanitize API environment variable option value
 *
 * @since 0.1.0
 *
 * @param mixed $value Value to sanitize.
 *
 * @return string
 */
function sanitize_api_env_var( $value ): string {
	$result = ! empty( $value )
		&& is_string( $value )
		&& preg_match( '!^cloudinary://[0-9]{15}:[A-Za-z0-9]{27}@[A-Za-z0-9_\-]{1,}!', $value ) === 1
			? $value
			: '';

	return $result;
}
