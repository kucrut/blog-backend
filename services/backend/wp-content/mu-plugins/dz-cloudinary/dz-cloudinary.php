<?php
/**
 * Plugin Name: Cloudinary
 * Description: Cloudinary Integration.
 * Version: 0.1.0
 * Author: Dzikri Aziz
 * Author URI: https://dz.aziz.im
 * License: GPL v2
 *
 * @package Dz_Cloudinary
 */

namespace Dz\Cloudinary;

require_once __DIR__ . '/inc/class-api.php';
require_once __DIR__ . '/inc/class-plugin.php';
require_once __DIR__ . '/inc/class-overrides.php';
require_once __DIR__ . '/inc/class-sync.php';
require_once __DIR__ . '/inc/cron.php';
require_once __DIR__ . '/inc/meta.php';
require_once __DIR__ . '/inc/settings.php';

Plugin::get_instance();
Meta\bootstrap();
Settings\bootstrap();
