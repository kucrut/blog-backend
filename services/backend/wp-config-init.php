<?php
/**
 * Extra configs for production environment
 *
 * @package Dz_Backend
 */

global $redis_server;

$cache_host = getenv( 'WP_CACHE_HOST' );
$cache_port = getenv( 'WP_CACHE_PORT' );

if ( $cache_host && $cache_port ) {
	$redis_server = [
		'host' => $cache_host,
		'port' => (int) $cache_port,
	];
}

define( 'DISABLE_WP_CRON', filter_var( getenv( 'DISABLE_WP_CRON' ), FILTER_VALIDATE_BOOLEAN ) );
