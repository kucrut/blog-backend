# Blog Backend

A WordPress site stack meant to be used as the backend for headless frontend.

## What's in this repo

- WordPress 6.0.2 on PHP-FPM 7.4.30/Alpine Linux
- Plugins:
  - [Two Factor](https://wordpress.org/plugins/two-factor/)
  - [WP Redis](https://wordpress.org/plugins/wp-redis/)
  - [Catatan](https://github.com/kucrut/catatan)
  - REST-ricted: Restrict REST API to logged in users only.
- Themes:
  - Hampa: A seriously blank theme.

## Prerequisite

Make sure you have [Docker](https://docs.docker.com/engine/installation/) and [Docker Compose](https://docs.docker.com/compose/) installed on your system and that the docker service is running.

## Local Setup

### Proxy

- Clone [proxy](https://gitlab.com/wp-id/docker/traefik-proxy) and enter the directory:
  ```sh
  git clone https://gitlab.com/wp-id/docker/traefik-proxy
  cd proxy
  ```
- Start the proxy:
  ```sh
  docker-compose up -d
  ```

### Site

TODO.
